module Lib where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

-- Exercise 1

fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x : xs)
  | even x = (x - 2) * fun1 xs
  | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' = product . map (subtract 2) . filter even

-- Exercise 2

-- Hint: For this problem you may wish to use the functions `iterate` and
-- `takeWhile`. Look them up in the Prelude documentation to see what they do.

fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n
  | even n = n + fun2 (n `div` 2)
  | otherwise = fun2 (3 * n + 1)
