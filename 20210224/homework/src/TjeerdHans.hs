module TjeerdHans where

--1
fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x : xs)
  | even x = (x - 2) * fun1 xs
  | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' [] = 1
fun1' xs = foldl (\acc x -> if even x then acc * (x -2) else acc) 1 xs

fun1'' :: [Integer] -> Integer
fun1'' [] = 1
fun1'' xs = product $ map (subtract 2) $ filter (even) xs

-- (-2) doesn't work because -2 is interpreted as a value. (+(-2)) would also work. https://stackoverflow.com/a/28858218

--2
fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n
  | even n = n + fun2 (n `div` 2)
  | otherwise = fun2 (3 * n + 1)

fun2' :: Integer -> Integer
fun2' 1 = 0
fun2' n = 0
