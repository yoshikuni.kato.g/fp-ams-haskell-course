import Prelude

type DiscAmount = Int

type From = String

type To = String

type Using = String

type Move = (From, To)

main :: IO ()
main = do
  putStrLn "Disc Amount?"
  amount <- readLn
  print $ hanoi amount "a" "c" "b"

hanoi :: DiscAmount -> From -> To -> Using -> [Move]
hanoi 0 _ _ _ = []
hanoi 1 from to _ = [(from, to)]
hanoi n from to using =
  concat
    [ hanoi next from using to,
      hanoi 1 from to using,
      hanoi next using to from
    ]
  where
    next = n -1
