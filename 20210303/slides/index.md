# Haskell: From Beginner to Intermediate #4

## FP AMS 03/03/2021 

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. **Basics**
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)

---

# Last Week

## Higher-Order Functions

- Currying
- Functions that take functions as parameters
- Functions that return functions
- Maps and filters
- Lambdas
- Folds
- Function application with `$`
- Function composition

---

# This Week

## Defining Your Own Types and Typeclasses

- Algebraic data types
- Record syntax
- Type parameters
- Derived instances
- Type synonyms
- Recursive data structures
- Typeclasses
- Kinds

---

# Homework Solutions

## Exercise 1

Reimplement in a more idiomatic Haskell style:

```haskell
fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x : xs)
  | even x = (x - 2) * fun1 xs
  | otherwise = fun1 xs
```

---

# Homework Solutions

## Exercise 1

Tjeerd Hans:

```haskell
fun1' :: [Integer] -> Integer
fun1' [] = 1
fun1' xs = product $ map (subtract 2) $ filter (even) xs
```

Even more idiomatic:

```haskell
fun1' :: [Integer] -> Integer
fun1' = product . map (subtract 2) . filter even
```

---

# This Week

## Defining Your Own Types and Typeclasses

- **Algebraic data types**
- Record syntax
- Type parameters
- Derived instances
- Type synonyms
- Recursive data structures
- Typeclasses
- Kinds

---

# Algebraic Data Types

Composite types:
- Product types (tuples and records)
- Sum types (tagged unions)

---

# Algebraic Data Types

## Sum Types

```haskell
data Bool = False | True
```

- keyword: `data`
- type: `Bool`
- value constructors: `False` and `True`
- note that value constructors are functions

---

# Algebraic Data Types

## Shapes (1/x)

```haskell
module Shapes (Shape (..)) where

data Shape
  = Circle Float Float Float
  | Rectangle Float Float Float Float

```

---

# Algebraic Data Types

## Shapes (1/x)

```haskell
module Shapes (Point (..), Shape (..)) where

data Point = Point Float Float
  deriving (Show)

data Shape
  = Circle Point Float
  | Rectangle Point Point
  deriving (Show)
```

---

# Algebraic Data Types

## Shapes (1/x)

```haskell
surface :: Shape -> Float
surface (Circle _ r) = pi * r ^ 2
surface (Rectangle (Point x1 y1) (Point x2 y2)) =
  abs (x2 - x1) * abs (y2 - y1)
```

---

# Algebraic Data Types

## Shapes (1/x)

```haskell
```

---

# Next Week
  
`TODO`

---

# Homework

`TODO`
