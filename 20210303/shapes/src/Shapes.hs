module Shapes
  ( Point (..),
    Shape (..),
    surface,
    move,
  )
where

data Point = Point Float Float
  deriving (Show)

data Shape
  = Circle Point Float
  | Rectangle Point Point
  deriving (Show)

surface :: Shape -> Float
surface (Circle _ r) = pi * r ^ 2
surface (Rectangle (Point x1 y1) (Point x2 y2)) =
  abs (x2 - x1) * abs (y2 - y1)

move :: Shape -> Float -> Float -> Shape
move (Circle (Point x y) r) dx dy =
  Circle (Point (x + dx) (y + dy)) r
move (Rectangle (Point x1 y1) (Point x2 y2)) dx dy =
  Rectangle (Point (x1 + dx) (y1 + dy)) (Point (x2 + dx) (y2 + dy))
